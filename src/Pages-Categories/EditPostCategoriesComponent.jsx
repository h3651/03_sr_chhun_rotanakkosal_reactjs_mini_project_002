import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { api } from "../API/api";

export default function EditPostCategoriesComponent() {
  const [name, setName] = useState("");
  const [id, setId] = useState();
  const navigate = useNavigate();

  const beforSubmit = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will update this category!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, save it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Save!", "Your file has been Save.", "success");
        handleSubmit();
        navigate("/categories");
      } else {
        return;
      }
    });
  };

  const handleSubmit = () => {
    api
      .put("/category/" + id, { name })
      .then((res) => Swal.fire("Good job!", res?.data?.message, "success"));
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const location = useLocation();

  useEffect(() => {
    setName(location.state?.name);
    setId(location.state?.id);
  }, [location]);

  return (
    <Container>
      <h1 style={{marginBottom:'20px'}}>Edit Category</h1>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Name</Form.Label>
          <Form.Control value={name} type="text" placeholder="Enter Category name"
            onChange={handleNameChange}/>
        </Form.Group>
        <Button variant="primary" onClick={beforSubmit}> Submit </Button>
      </Form>
    </Container>
  );
}