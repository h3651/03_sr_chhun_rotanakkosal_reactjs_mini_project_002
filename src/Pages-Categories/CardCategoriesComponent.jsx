import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardCategoriesComponent({ item, handleDelete }) {
  const navigate = useNavigate();

  const onEdit = (name, id) => {
    navigate("/categories/edit", { state: { name, id} });
  };

  const onView = () => {
    navigate("/categories/view", { state: { ...item } });
  };
  
    const cardStyle = {
      height : '300px',
      border: '1px solid orangered',
      borderRadius : '25px',
      marginRight : '50px'
    };

    const titleSize = {
      marginTop : '10px',
      textAlign : 'center',
      height:"65px",
      fontWeight: 'bold'
    }
    const button = {
        width : '9.5rem',
        margin : '0px 20px 20px 20px '  
    }

 return (
    <div>
     <Card style={cardStyle}>

        <Card.Body className="p-2">
          
          <Card.Title className="titleLine" style={ titleSize } >{item.name}</Card.Title>
          
            <Button style={button} variant="primary"
                    onClick={() => onEdit(item.name, item._id)}>edit
            </Button>

            <Button className="btnCate" style={button} variant="warning" 
                    onClick={onView}> view
            </Button>
            
            <Button style={button} variant="danger" 
                    onClick={() => handleDelete(item._id)} > delete
            </Button>

        </Card.Body>
      </Card>
    </div>
  );
}
