import React from "react";
import { Button, Container, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

export default function ViewCardCategoryComponent() {

  const location = useLocation();
  const oldData = location.state;
  console.log(oldData);

  const navigate = useNavigate();
  return (
    <div>
      <Container className="my-2">
        <Row className="view-card p-3">
          <Button onClick={() => navigate("/categories")} style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"> Back
          </Button>

          <div style={{ display: "flex", margin: 5, height: "500px" }}>
            <h1 className="p-3">Category Name: </h1>
            <h1 className="p-3"> {oldData?.name}</h1>
          </div>

        </Row>
      </Container>
    </div>
  );
}