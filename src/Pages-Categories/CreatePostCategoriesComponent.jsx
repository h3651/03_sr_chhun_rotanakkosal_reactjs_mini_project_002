import React, { useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { api } from "../API/api";

export default function CreatePostCategoriesComponent() {
  const [name, setName] = useState("");
 
  const handleSave = () => {
    api
      .post("/category",{name})
      .then((res) => console.log(res.data.message))
      .then(Swal.fire({
        title: 'Are you sure?',
        text: "You will save this article and it will show publicly",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, save it!'
      })
      .then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Data has been posted successfully',
            '',
            'success' )
        }
      }).then(function() {
        window.location = "/categories";}
      ))
  };

  const handleName = (e) => {
    setName(e.target.value);
  };
  console.log(name)
  
  return (
    <div>
      <Container>
        <div className="spanSize"> Add New Article </div>
        <Form>

          <Form.Group className="mb-3">
            <Form.Label>name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" onChange={handleName} />
          </Form.Group>         

          <Button variant="primary" onClick={handleSave}> Save </Button>

        </Form>
      </Container>
    </div>
  );
}
