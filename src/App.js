
import { Route, Routes } from 'react-router-dom';
import './App.css';
import NavbarComponent from './Component/NavbarComponent';
import CreatePostComponent from './Pages-Articles/CreatePostComponent';
import EditPostComponent from './Pages-Articles/EditPostComponent';
import HomeComponent from './Pages-Articles/HomeComponent';
import ViewCardComponent from './Pages-Articles/ViewCardComponent';
import CreatePostCategoriesComponent from './Pages-Categories/CreatePostCategoriesComponent';
import EditPostCategoriesComponent from './Pages-Categories/EditPostCategoriesComponent';
import HomeCategoriesComponent from './Pages-Categories/HomeCategoriesComponent';
import ViewCardCategoryComponent from './Pages-Categories/ViewCardCategoriesComponent';

function App() {
  return (
    <div className="App">
      <NavbarComponent/>
      <Routes>
        <Route path='/' element={<HomeComponent/>}/>
        <Route path='/categories' element={<HomeCategoriesComponent/>}/>
        <Route path='/addarticle' element={<CreatePostComponent/>}/>
        <Route path='/addcategories' element={<CreatePostCategoriesComponent/>}/>
        <Route path="/view" element={<ViewCardComponent />} />
        <Route path="/categories/view" element={<ViewCardCategoryComponent/>}/>
        <Route path="/edit" element={<EditPostComponent/>} />
        <Route path="/categories/edit" element={<EditPostCategoriesComponent/>} />

      </Routes>
    </div>
  );
}

export default App;
