import React from "react";
import { Button, Container, Image, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

export default function ViewCardComponent() {
  const location = useLocation();
  const oldData = location.state;
  console.log(oldData);
  const navigate = useNavigate();
  return (
    <div>
      <Container className="my-2" style={{border:'2px solid orangered',height:'800px'}}>
        <Row className="p-3" style={{height:'600px'}}>

          <Button onClick={() => navigate("/")} style={{ width: "10%" }}
                  className="m-3 btn-light fw-bold" > Back
          </Button>

          <div style={{ display: "flex", margin: 5, height: "600px" }} >
            <Image className="imgShow" src={oldData.image ??
                "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"}/>
            
            <div className="imgSize">
                <h4 style={{marginLeft:'20px'}} className="p-3"> Title : {oldData?.title}</h4>
                <div className="p-3 fontSize pStyle">
                           Description : {oldData?.description}
                </div>
                <div className="p-3 fontSize" style={{marginLeft:'20px'}}>
                     Published : {oldData?.published ? "True" : "False"}
                </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
}