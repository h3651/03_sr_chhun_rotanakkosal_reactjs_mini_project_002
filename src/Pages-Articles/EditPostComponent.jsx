import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { api } from "../API/api";

export default function EditPostComponent() {
  const [id, setId] = useState();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isPublished, setIsPublished] = useState(true);
  const [image, setImage] = useState("");
  const [imageUrl, setImageUrl] = useState();
  const [category, setCategory] = useState("");

  const navigate = useNavigate();

  const beforSubmit = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will saved this article and show it in public!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, save it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Save!", "Your file has been Save.", "success");
        handleSubmit();
        navigate("/");
      } else {
        return;
      }
    });
  };

  const handleSubmit = () => {
    api
      .patch("/articles/" + id, {
        title,
        description,
        isPublished,
        image,
        category,
      })
      .then((res) => Swal.fire("Good job!", res?.data?.message, "success"));
  };

  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };

  useEffect(() => {
    console.log(title);
  }, [title]);

  const handleDesChange = (e) => {
    setDescription(e.target.value);
  };

  const isPublishedChange = (e) => {
    setIsPublished(e.target.checked);
    console.log(e.target.checked);
  };

  const handleImageChange = (e) => {
    console.log("e.target.files[0] " + e.target.files[0]);
    console.log(URL.createObjectURL(e.target.files[0]));
    setImageUrl(URL.createObjectURL(e.target.files[0]));
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    console.log("FormData : ", formData.get("image"));
    api
      .post("/images", formData)
      .then((res) => setImage(res?.data?.payload?.url));
  };

  const location = useLocation();
  const oldData = location.state;
  useEffect(() => {
    setId(oldData._id);
    setTitle(oldData.title);
    setDescription(oldData.description);
    setImage(
      oldData.image ??
        "https://www.pulsecarshalton.co.uk/wp-content/uploads/2016/08/jk-placeholder-image.jpg",
    );
    setCategory(oldData.category);
    setIsPublished(oldData.isPublished);
  }, [location]);

  return (
    <Container className="w-50">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            value={title}
            placeholder="title"
            onChange={handleTitleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            value={description}
            placeholder="description"
            onChange={handleDesChange}
          />
        </Form.Group>
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Image File</Form.Label>
          <Form.Control type="file" onChange={handleImageChange} />
        </Form.Group>
        <img
          src={imageUrl ?? image}
          alt="preview"
          style={{ height: "200px" }}
        />
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check
            type="checkbox"
            checked={isPublished}
            label="Is Published"
            onChange={isPublishedChange}
          />
        </Form.Group>
        <Button variant="danger mx-2" as={Link} to="/">
          Canncel
        </Button>
        <Button variant="primary" onClick={beforSubmit}>
          Submit
        </Button>
      </Form>
    </Container>
  );
}