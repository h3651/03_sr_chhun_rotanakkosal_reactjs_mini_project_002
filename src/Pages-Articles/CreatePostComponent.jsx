import React, { useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { api } from "../API/api";

export default function CreatePostComponent() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isPublished, setIsPublished] = useState(true);
  const [image, setImage] = useState("");
  const [imageUrl, setImageUrl] = useState();

  const handleSubmit = () => {
    api
      .post("/articles", { title, description, isPublished, image })
      .then((res) => console.log(res.data.message))
      .then(Swal.fire({
        title: 'Are you sure?',
        text: "You will save this article and it will show publicly",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, save it!'
      })
      .then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Data has been posted successfully',
            '',
            'success' )
        }
      }).then(function() {
        window.location = "/";}
      ))
  };

  const handleTitle = (e) => {
    setTitle(e.target.value);
  };
  console.log(title)

  const handleDescription = (e) => {
    setDescription(e.target.value);
  };
  console.log(description)

  const isPublishedChange = (e) => {
    setIsPublished(e.target.checked);
    console.log(e.target.checked);
  };
  
  const handleImage = (e) => {
    setImageUrl(URL.createObjectURL(e.target.files[0]));
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    console.log("FormData : ", formData.get("image"));
    api.post("/images", formData).then((res) => setImage(res.data.payload.url));
  };
  
  return (
    <div>
      <Container>
        <div className="spanSize"> Add New Article </div>
        <Form>

          <Form.Group className="mb-3">
            <Form.Label>Title</Form.Label>
            <Form.Control type="text" placeholder="Enter title" onChange={handleTitle} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>Description</Form.Label>
            <Form.Control type="text" placeholder="Enter description" onChange={handleDescription} />
          </Form.Group>

          <Form.Group className="imgMar" controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox" label="Is Published" onChange={isPublishedChange} />
          </Form.Group>

          <div className="imgMar size border">
              <img className="size" src={ imageUrl ?? "https://media.istockphoto.com/vectors/default-avatar-photo-placeholder-icon-grey-profile-picture-business-vector-id1327592506?k=20&m=1327592506&s=612x612&w=0&h=hgMOPfz7H-CYP_CQ0wbv3IwRkbQna32xWUPoXtMyg5M="} 
               alt="preview"/>
          </div>

          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label >Image File</Form.Label>
            <Form.Control type="file" onChange={handleImage} />
          </Form.Group>

          <span className="button">
              <Button style={{marginRight:'20px'}} variant="danger" as={Link} to='/'> Cancel </Button>
              <Button variant="primary" onClick={handleSubmit}> Submit </Button>
          </span>
         
        </Form>
      </Container>
    </div>
  );
}
