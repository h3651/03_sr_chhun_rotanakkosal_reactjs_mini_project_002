import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { api } from "../API/api"
import CardComponent from "../Component/CardComponent";


export default function HomeComponent() {
  const [data, setData] = useState([]);
  
   useEffect(() => {
    api.get("/articles").then((res) => {
      setData(res.data.payload);
      console.log(res);
    });
  }, []);

  const handleDelete = async (id) => {
    await api.delete(`/articles/${id}`).then((res) => {console.log(res.data.message);})
    .then(
      Swal.fire({
        title: 'Do you want to delete the article ?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Delete',
        denyButtonText: `No`,
      })
      .then((result) => {
        if (result.isConfirmed) {
          Swal.fire('The article has been deleted successfully', '', 'success')
          const newData = data.filter(data => data._id !== id)
          setData(newData)
        } else if (result.isDenied) {
          Swal.fire('The article is not deleted', '', 'info')
        }
      })
    )
  }

  return (
    <div>   
      <Container>       
        <Row>

            <span className="spanSize">
              All Articles
              <Button variant="primary" size="lg" style={{float:'right'}} as={Link} to='/addarticle'>Add New Article</Button>
            </span>

          {data.map((item, index) => (
            <Col className="col-2" key={index} style={{marginTop:'50px'}}>
              <CardComponent item={item} handleDelete = { handleDelete } 
              />
            </Col>))}

        </Row>
      </Container>

    </div>
  );
}
