import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardComponent({ item, handleDelete, handleUpdate, hnadleViewById }) {
    const navigate = useNavigate();
    
    const onEdit = () => {
      navigate("/edit",{ state:{ ...item }})
    }
    
    const onView = () => {
      navigate("/view",{ state:{ ...item }})
    }
  const cardStyle = {
      width : '18remn',
      height : '690px',
      border: '1px solid orangered',
      borderRadius : '25px'};

    const picSize = {
      width : '9.4rem',
      height : '250px',
      borderRadius : '25px',
      objectFit: 'cover'
    }
    const titleSize = {
      marginTop : '20px',
      marginBottom : '20px',
      textAlign : 'center',
      height:"35px",
      fontWeight: 'bold'
    }
    const descSize = {
      marginBottom : '20px',
      textAlign : 'center',
      height:"124px",
      fontWeight: 'bold'
    }
 return (
    <div>
     <Card style={cardStyle}>

        <Card.Img className="p-2" style={picSize} variant="top" 
           src={item.image ?? "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"} 
        />
        <Card.Body>
          <Card.Title className="titleLine" style={ titleSize } >{item.title ?? "No Title"}</Card.Title>
          <Card.Text className="desLine" style={ descSize }> 
              {item.description ?? "No Description"} 
          </Card.Text>

          <Button style={{width:'7.5rem',marginBottom:"10px"}} variant="primary" onClick={() => onEdit(item._id)}> 
            edit 
          </Button>

          <Button style={{width:'7.5rem',marginBottom:"10px"}} onClick={onView} variant="warning">
            view
          </Button>
          <Button style={{width:'7.5rem'}} variant="danger" 
                    onClick={() => handleDelete(item._id)}> delete
          </Button>

        </Card.Body>
      </Card>
    </div>
  );
}
