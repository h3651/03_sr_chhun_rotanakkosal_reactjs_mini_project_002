import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function NavbarComponent() {
  return (
    <div>
        <Navbar bg="light" variant="light" style={{marginBottom:'60px'}}>
            <Container>
                <Navbar.Brand as={Link} to='/' style={{fontSize:'x-large'}}>React-bootstrap</Navbar.Brand>
                    <Nav>
                        <Nav.Link className='fontSize' as={Link} to='/'> Home </Nav.Link>
                        <Nav.Link className='fontSize' as={Link} to='/categories'> Categories </Nav.Link>
                    </Nav>
            </Container>
        </Navbar>
    </div>
  )
}
